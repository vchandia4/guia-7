from proteina import Proteina 

class SecuenciaProteica():
    def __init__(self, lista):
        self.__lista = lista

    def get_lista(self):
        return self.__lista

    def set_mutacion(self, lista):
        if isinstance(lista, str):
            self.__lista = lista
        else:
            print("la mutacion no ha sido posible de agregar")

    def mostrar_lista(self):
        for i in self.__lista:
            print(f"{i.get_nombre(),i.get_descripcion(),i.get_secuencia()}")

    def mostrar_secuencia(self):
        print( f"{self.get_secuencia()}")