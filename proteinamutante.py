from proteina import Proteina 

class ProteinaMutante(Proteina):
    def __init__(self, nombre, descripcion, secuencia, mutacion):
        super().__init__(nombre, descripcion, secuencia)
        self.__mutacion = mutacion
    


    def get_mutacion(self):
        return self.__mutacion

    def set_mutacion(self, mutacion):
        if isinstance(mutacion, str):
            self.__mutacion = mutacion
        else:
            print("la mutacion no ha sido posible de agregar")

    def mostrar_mutacion(self):
        print( f"mutacion:{self.get_mutacion()}")