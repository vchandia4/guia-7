from proteina import Proteina 


class ProteinaEnzimatica(Proteina):
    def __init__(self, nombre, descripcion, secuencia, substrato):
        super().__init__(nombre, descripcion, secuencia)
        self.__substrato = substrato



    def get_substrato(self):
        return self.__substrato

    def set_substrato(self, substrato):
        if isinstance(substrato, str):
            self.__substrato = substrato
        else:
            print("El substrato no ha sido posible de agregar")


    def mostrar_substrato(self):
        print( f"el tipo de sustrato es {self.get_substrato()}")
