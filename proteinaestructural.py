from proteina import Proteina 

class ProteinaEstructural(Proteina):
    def __init__(self, nombre, descripcion, secuencia, tipo):
        super().__init__(nombre, descripcion, secuencia)
        self.__tipo = tipo


    def get_tipo(self):
        return self.__tipo

    def set_tipo(self, tipo):
        if isinstance(tipo, str):
            if tipo == "fibrosa":
                self.__tipo = tipo
            elif tipo == "globular":
                self.__tipo = tipo
        else:
            print("El tipo no ha sido posible de agregar")

    def mostrar_tipo(self):
        print( f"el tipo de proteina es {self.get_tipo()}")
